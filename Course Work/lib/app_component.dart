// Copyright (c) 2017, Andrew. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'models/log.dart';
import 'services/logging_service.dart';

import 'services/delivery.dart';
import 'services/delivery_service.dart';

import 'package:Course_Work/components/create_cargo/create_cargo.dart';
import 'package:Course_Work/components/logging_panel/logging_panel.dart';
import 'package:Course_Work/components/deliveries_panel/deliveries_panel.dart';
import 'package:Course_Work/components/active_deliveries_panel/active_deliveries_panel.dart';
import 'package:Course_Work/components/delivery_history_panel/delivery_history_panel.dart';

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [materialDirectives, CreateCargo, LoggingPanel, DeliveriesPanel, ActiveDeliveriesPanel, DeliveryHistoryPanel],
  providers: const [materialProviders],
)
class AppComponent implements OnInit {

  List<Log> getLogs() {
    return LoggingService.logs;
  }

  List<Delivery> getDeliveries() {
    return DeliveryService.deliveries.where((d) => d.state == DeliveryState.Waits);
  }

  List<Delivery> getActiveDeliveries() {
    return DeliveryService.deliveries.where((d) => d.state == DeliveryState.Sent);
  }

  List<Delivery> getDeliveredDeliveries() {
    return DeliveryService.deliveries.where((d) => d.state == DeliveryState.Delivered);
  }

  @override
  ngOnInit() {}
}
