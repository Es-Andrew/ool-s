import 'package:Course_Work/models/log.dart';

class LoggingService {
  static List<Log> _logs = new List<Log>();

  static void addLog(Log log) {
    _logs.add(log);
  }

  static void sendLog(String message) {
    LoggingService.addLog(new Log(message));
  }

  static void removeLog(Log log) {
    try {
      _logs.remove(log);
    } catch (e) {
      print(e);
    }
  }

  static void removeAllLogs() {
    _logs.clear();
  }

  static List<Log> get logs => _logs;
}
