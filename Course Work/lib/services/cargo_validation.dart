class CargoValidation {
  static bool isCargoValid(String from, String to, num weight) {
    return _stringValidation([from, to]) && _numValidation(weight);
  }

  static bool _stringValidation(List<String> words) {
    bool isValid = true;
    RegExp regExp = new RegExp("[А-яа-яA-Za-z0-9&#@]{2,}");
    for(String word in words)
      if(!regExp.hasMatch(word)) isValid = false;
    return isValid;
  }

  static bool _numValidation(num number) {
    return number is num ? number > 0 : false;
  }
}