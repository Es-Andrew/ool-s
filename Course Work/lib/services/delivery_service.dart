import 'transport_service.dart';

import 'transport.dart';
import 'delivery.dart';
import 'logging_service.dart';

import 'package:Course_Work/models/cargo.dart';
import 'package:Course_Work/models/track.dart';
import 'package:Course_Work/models/log.dart';

class DeliveryService {
  static List<Delivery> _deliveries = new List<Delivery>();

  static void addDelivery(Cargo cargo, [TransportType type = null]) {
    if (hasWaitingDelivery(cargo, type)) {
      _getWaitingDelivery(cargo, type).addCargo(cargo);
      _sendLog("Груз [${cargo.name}] добавлен в уже существующий заказ");
    } else if (TransportService.HasFreeTransport(cargo, type)) {
      Delivery delivery =
          new Delivery(TransportService.getTransport(cargo, type), new Track());
      delivery.addCargo(cargo);
      _deliveries.add(delivery);
      _sendLog("Создан новый заказ [${cargo.name}]");
    } else
      _sendLog(
          "Заказ [${cargo.name}] не потвержден, нет подходящего транспортного средства");
  }

  static bool hasWaitingDelivery(Cargo cargo, TransportType type) {
    if (type == null) return deliveries.any((d) => d.canAddCargo(cargo));
    return deliveries.any((d) =>
        d.canAddCargo(cargo) &&
        type.toString().split('.')[1] == d.transport.runtimeType.toString());
  }

  static Delivery _getWaitingDelivery(Cargo cargo, TransportType type) {
    if (type == null) return deliveries.firstWhere((d) => d.canAddCargo(cargo));
    return deliveries.firstWhere((d) =>
        d.canAddCargo(cargo) &&
        type.toString().split('.')[1] == d.transport.runtimeType.toString());
  }

  static void removeDelivery(Delivery delivery) {
    _deliveries.removeWhere((d) => d.name == delivery.name);
  }

  static void _sendLog(String message) {
    LoggingService.addLog(new Log(message));
  }

  static List<Delivery> get deliveries => _deliveries;

  static int get getDeliveriesCount => _deliveries.length;
}
