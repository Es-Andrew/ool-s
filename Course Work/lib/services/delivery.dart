import 'dart:async';
import 'dart:math';

import 'package:Course_Work/models/track.dart';
import 'package:Course_Work/models/cargo.dart';

import 'transport.dart';

import 'logging_service.dart';

enum DeliveryState { Waits, Sent, Delivered }

class Delivery {
  Transport _transport;
  DeliveryState _state;
  Track _track;
  List<Cargo> _cargo;
  String _name;

  static int _count = 0;
  static Duration _tick = new Duration(milliseconds: 30);

  Timer _timer;

  static Random _random = new Random();

  Delivery(this._transport, this._track) {
    _initialize();
  }

  void _initialize() {
    _cargo = new List<Cargo>();
    _state = DeliveryState.Waits;
    _count++;
  }

  void addCargo(Cargo cargo) {
    if (canAddCargo(cargo)) {
      _setName(cargo);
      _transport.addCargo(cargo);
      _cargo.add(cargo);
    }
  }

  bool canAddCargo(Cargo cargo) {
    return !_cargo.any((c) => c.from != cargo.from || c.to != cargo.to) &&
        _transport.weight + cargo.weight <= _transport.maxWeight &&
        state == DeliveryState.Waits;
  }

  void _setName(Cargo cargo) {
    _name = _genName(cargo, 4);
  }

  String _genName(Cargo cargo, int numberLength) {
    String name = cargo.from[0] + cargo.to[cargo.to.length - 1];
    for (int i = 0; i < numberLength; i++)
      name += _random.nextInt(10).toString();
    name += "-" +
        _count.toString() +
        cargo.to[0] +
        cargo.from[cargo.from.length - 1];
    return name;
  }

  void start() {
    _timer = new Timer.periodic(_tick, (event) => _makeNextStep());
    _transport.start();
    _state = DeliveryState.Sent;
  }

  void _makeNextStep() {
    if (!track.isEnd()) {
      _track.addDistance(_transport.speed);
    } else {
      stop();
      sendDeliveredLog();
      _state = DeliveryState.Delivered;
      _transport.removeAllCargo();
    }
  }

  void sendDeliveredLog() {
    LoggingService.sendLog(
        "Заказ [$name] доставлен транспортным средством [${transport.name}]");
  }

  void stop() {
    _transport.stop();
    _timer.cancel();
  }

  num getWeight() {
    num weight = 0;
    for (Cargo cargo in cargo) weight += cargo.weight;
    return weight;
  }

  Transport get transport => _transport;
  DeliveryState get state => _state;
  Track get track => _track;
  List<Cargo> get cargo => _cargo;
  String get name => _name;
}
