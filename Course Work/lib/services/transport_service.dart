import 'dart:math';

import 'transport.dart';
import 'package:Course_Work/models/cargo.dart';

class TransportService {
  static Random _random = new Random();
  static List<Transport> _transports = _genTransports(15);

  static List<Transport> _genTransports(int count) {
    List<Transport> outputTransport = new List<Transport>();
    for (int i = 0; i < count; i++)
      outputTransport
          .add(_genTransport(_random.nextInt(TransportType.values.length)));
    return outputTransport;
  }

  static Transport _genTransport(int number) {
    return Transport.CreateTransport(TransportType.values[number]);
  }

  static bool HasFreeTransport(Cargo cargo, [TransportType type = null]) {
    if (type == null)
      return _transports
          .any((t) => t.state == TransportState.Free && t.canAddCargo(cargo));
    return _transports.any((t) =>
        t.runtimeType.toString() == type.toString().split('.')[1] &&
        t.state == TransportState.Free &&
        t.canAddCargo(cargo));
  }

  static List<Transport> _getTransports(Cargo cargo, TransportType type) {
    return _transports.where((t) =>
        t.state == TransportState.Free &&
        t.runtimeType.toString() == type.toString().split('.')[1] &&
        t.canAddCargo(cargo));
  }

  static List<Transport> _getAnyTransports(Cargo cargo) {
    return _transports
        .where((t) => t.state == TransportState.Free && t.canAddCargo(cargo));
  }

  static Transport getTransport(Cargo cargo, [TransportType type = null]) {
    if (type == null) return _getAnyTransports(cargo).first;
    return _getTransports(cargo, type).first;
  }

  static TransportType _getRandomTransportType() {
    return TransportType.values[_random.nextInt(TransportType.values.length)];
  }
}
