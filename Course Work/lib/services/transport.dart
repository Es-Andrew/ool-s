import 'dart:math';
import 'dart:async';

import 'package:bbt/bbt.dart';

import 'package:Course_Work/models/cargo.dart';

enum TransportType { Vehicle, Ship, Aircraft }

enum TransportState { Moving, Refueling, Free }

abstract class Transport {
  num _speed,
      _maxSpeed,
      _fuel,
      _refuelingSpeed,
      _fuelReduction,
      _maxFuel,
      _weight,
      _maxWeight;
  String _name;
  TransportState _state;

  List<Cargo> _cargo;

  static Random _random = new Random();

  static int _count = 0;
  static Duration _tick = new Duration(milliseconds: 30);

  Timer _timer;

  static Transport CreateTransport(TransportType type) {
    switch (type) {
      case TransportType.Vehicle:
        return new Vehicle();
      case TransportType.Ship:
        return new Ship();
      case TransportType.Aircraft:
        return new Aircraft();
      default:
        throw new ArgumentError("Incorrect input value");
    }
  }

  Transport() {
    _initialize();
  }

  bool hasCargo() {
    return cargo.length > 0;
  }

  bool canAddCargo(Cargo cargo) {
    return !_cargo.any((c) =>
            c.name == cargo.name || c.from != cargo.from || c.to != cargo.to) &&
        (weight + cargo.weight <= maxWeight);
  }

  void addCargo(Cargo cargo) {
    if (canAddCargo(cargo)) {
      _cargo.add(cargo);
      _setWeight = weight + cargo.weight;
    }
  }

  void removeCargo(Cargo cargo) {
    if (_cargo.any((c) => c.name == cargo.name)) {
      _cargo.removeWhere((c) => c.name == cargo.name);
      _setWeight = weight - cargo.weight;
    }
  }

  void removeAllCargo() {
    _cargo.clear();
    _setWeight = 0;
  }

  void _initialize() {
    _setMaxSpeed();
    _setMaxFuel();
    _setRefuelingSpeed();
    _setFuelReduction();
    _setMaxWeight();
    _setName();

    _weight = 0;
    _fuel = maxFuel;
    _setSpeed = 0;
    _cargo = new List<Cargo>();
    _state = TransportState.Free;
    _count++;
  }

  void _setMaxSpeed() {
    _maxSpeed = _genMaxSpeed() /
        (new Duration(seconds: 1).inMilliseconds / _tick.inMilliseconds);
  }

  num _genMaxSpeed();

  void _setMaxFuel() {
    _maxFuel = _genMaxFuel();
  }

  num _genMaxFuel();

  void _setRefuelingSpeed() {
    _refuelingSpeed = _genRefuelingSpeed() /
        (new Duration(seconds: 1).inMilliseconds / _tick.inMilliseconds);
  }

  num _genRefuelingSpeed();

  void _setFuelReduction() {
    _fuelReduction = _genFuelReduction() /
        (new Duration(seconds: 1).inMilliseconds / _tick.inMilliseconds);
  }

  num _genFuelReduction();

  void _setMaxWeight() {
    _maxWeight = _genMaxWeight();
  }

  num _genMaxWeight();

  void _setName() {
    _name = _genName();
  }

  String _genName();

  void start() {
    _state = TransportState.Moving;
    _timer = new Timer.periodic(_tick, (Timer timer) => _move());
  }

  void _move() {
    if (hasFuel() && state != TransportState.Refueling) {
      _setSpeed = _getCorrectSpeed();
      _fuel -= fuelReduction;
    } else {
      _refill();
    }
  }

  bool hasFuel() {
    return _fuel > ((maxFuel * 10) / 100);
  }

  num _getCorrectSpeed() {
    return _getRandomValue(maxSpeed - _getPercentValue(maxSpeed, 20), maxSpeed);
  }

  void _refill() {
    _speed = 0;
    _state = TransportState.Refueling;
    if (!isFilledFuel())
      _fuel = _getCorrectFuel();
    else
      _state = TransportState.Moving;
  }

  bool isFilledFuel() {
    return fuel == maxFuel;
  }

  num _getCorrectFuel() {
    return min(fuel + refuelingSpeed, maxFuel);
  }

  void stop() {
    _timer.cancel();
    _setSpeed = 0;
    _state = TransportState.Free;
  }

  String getTransportState();

  num _getRandomValue(num from, num to) {
    return new randflat(xa: from, xb: to, seed: _random.nextInt(_count + 2))
        .Fire();
  }

  num _getPercentValue(num number, num percent) {
    return (number * percent) / 100;
  }

  String _getDefaultName(String base, [String postfix = ""]) {
    if (postfix == "") postfix = _count.toString();
    return "$base-$postfix";
  }

  num get speed => _speed;

  set _setSpeed(num speed) => _speed = min(speed, maxSpeed);

  num get weight => _weight;

  set _setWeight(num weight) => _weight = min(weight, maxWeight);

  num get maxSpeed => _maxSpeed;
  num get fuel => _fuel;
  num get maxFuel => _maxFuel;
  num get refuelingSpeed => _refuelingSpeed;
  num get fuelReduction => _fuelReduction;
  num get maxWeight => _maxWeight;
  String get name => _name;
  TransportState get state => _state;
  List<Cargo> get cargo => _cargo;
}

class Vehicle extends Transport {
  @override
  num _genMaxSpeed() {
    return _getRandomValue(75, 120);
  }

  @override
  num _genMaxFuel() {
    return _getRandomValue(2, 6);
  }

  @override
  num _genFuelReduction() {
    return _getRandomValue(0.1, 0.4);
  }

  @override
  num _genRefuelingSpeed() {
    return _getRandomValue(0.5, 1.1);
  }

  @override
  num _genMaxWeight() {
    return _getRandomValue(250, 1800);
  }

  @override
  String _genName() {
    return _getDefaultName("Машина");
  }

  @override
  String getTransportState() {
    switch (state) {
      case TransportState.Moving:
        return "Едет";
      case TransportState.Refueling:
        return "Заправляется";
      default:
        return "Ждёт";
    }
  }
}

class Ship extends Transport {
  @override
  num _genMaxSpeed() {
    return _getRandomValue(20, 80);
  }

  @override
  num _genMaxFuel() {
    return _getRandomValue(1200, 2000);
  }

  @override
  num _genFuelReduction() {
    return _getRandomValue(10, 45);
  }

  @override
  num _genRefuelingSpeed() {
    return _getRandomValue(30, 55);
  }

  @override
  num _genMaxWeight() {
    return _getRandomValue(3600, 5000);
  }

  @override
  String _genName() {
    return _getDefaultName("Судно");
  }

  @override
  String getTransportState() {
    switch (state) {
      case TransportState.Moving:
        return "Плывёт";
      case TransportState.Refueling:
        return "Заправляется";
      default:
        return "Ждёт";
    }
  }
}

class Aircraft extends Transport {
  @override
  num _genMaxSpeed() {
    return _getRandomValue(350, 620);
  }

  @override
  num _genMaxFuel() {
    return _getRandomValue(220, 545);
  }

  @override
  num _genFuelReduction() {
    return _getRandomValue(5, 15);
  }

  @override
  num _genRefuelingSpeed() {
    return _getRandomValue(60, 120);
  }

  @override
  num _genMaxWeight() {
    return _getRandomValue(2000, 3250);
  }

  @override
  String _genName() {
    return _getDefaultName("Самолёт");
  }

  @override
  String getTransportState() {
    switch (state) {
      case TransportState.Moving:
        return "Летит";
      case TransportState.Refueling:
        return "Заправляется";
      default:
        return "Ждёт";
    }
  }
}
