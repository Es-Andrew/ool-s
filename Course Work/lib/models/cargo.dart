class Cargo {
  num _weight;

  String _from;
  String _to;

  String _name;

  static int _count = 0;

  Cargo(this._from, this._to, this._weight) {
    _initialize();
  }

  void _initialize() {
    _setName();

    _count++;
  }

  void _setName() {
    _name = _genName();
  }

  String _genName() {
    return from[0] + to[0] + "$_count-$weight-" + new DateTime.now().toIso8601String();
  }

  num get weight => _weight;

  String get from => _from;
  String get to => _to;

  String get name => _name;
}

