class Log {
  String _message;
  DateTime _time;

  Log(String message) {
    if (_isValidMessage(message))
      _message = message;
    else
      throw ArgumentError;
    _time = new DateTime.now();
  }

  bool _isValidMessage(String message) {
    return message.length > 0;
  }

  String getShortTime() {
    String shortTime = "${_time.year}-${_time.month}-${_time.day}" +
        ":${_time.hour}-${_time.minute}-${_time.second}";
    return shortTime;
  }

  String get message => _message;
  DateTime get time => _time;
}
