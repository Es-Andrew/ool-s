import 'dart:math';

import 'package:bbt/bbt.dart';

class Track {
  num _currentDistance;
  num _endDistance;

  static Random _random = new Random();

  static int _count = 0;

  Track([num endDistance = 0]) {
    _initialize(endDistance);
  }

  void _initialize(num endDistance) {
    _setEndDistance(endDistance);
    _currentDistance = 0;
    _count++;
  }

  void _setEndDistance(num endDistance) {
    if(endDistance == 0) _endDistance = _genDistance();
    else _endDistance = endDistance;
  }

  num _genDistance() {
    return new randflat(xa: 75.0, xb: 2500.0, seed: _random.nextInt(_count + 2)).Fire();
  }

  void addDistance(num distance) {
    _setCurrentDistance = currentDistance + distance;
  }

  bool isEnd() {
    return _currentDistance == _endDistance;
  }

  num get currentDistance => _currentDistance;

  set _setCurrentDistance(num distance) => _currentDistance = min(distance, endDistance);

  num get endDistance => _endDistance;
}