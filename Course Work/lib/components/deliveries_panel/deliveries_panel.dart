import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/services/delivery.dart';

import 'package:Course_Work/components/delivery_panel/delivery_panel.dart';

import 'package:Course_Work/components/WindowManager.dart';

@Component(
    selector: 'deliveries-panel',
    styleUrls: const ['deliveries_panel.css'],
    templateUrl: 'deliveries_panel.html',
    directives: const [materialDirectives, DeliveryPanel],
    providers: const [materialProviders])
class DeliveriesPanel extends WindowManager implements OnInit {
  @Input()
  List<Delivery> deliveries;

  @ViewChild('content')
  ElementRef content;

  @override
  void onResize() {
    _getNativeElement(content).style.maxHeight =
        (document.body.clientHeight - _getNativeElement(content).offset.top)
                .toString() +
            "px";
  }

  Element _getNativeElement(ElementRef element) {
    return element.nativeElement;
  }

  @override
  ngOnInit() {
    onResize();
  }
}
