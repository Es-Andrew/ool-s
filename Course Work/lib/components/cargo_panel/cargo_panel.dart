import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/models/cargo.dart';

@Component(
    selector: 'cargo-panel',
    styleUrls: const ['cargo_panel.css'],
    templateUrl: 'cargo_panel.html',
    directives: const [materialDirectives],
    providers: const [materialProviders])
class CargoPanel {
  @Input()
  Cargo cargo;
}
