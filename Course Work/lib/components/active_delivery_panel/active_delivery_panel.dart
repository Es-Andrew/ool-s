import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/services/delivery.dart';
import 'package:Course_Work/components/cargo_panel/cargo_panel.dart';

@Component(
    selector: 'active-delivery-panel',
    styleUrls: const ['active_delivery_panel.css'],
    templateUrl: 'active_delivery_panel.html',
    directives: const [materialDirectives, CargoPanel],
    providers: const [materialProviders])
class ActiveDeliveryPanel {
  @Input()
  Delivery delivery;

  int getProgressValue() {
    return ((delivery.track.currentDistance * 100) / delivery.track.endDistance).toInt();
  }
}
