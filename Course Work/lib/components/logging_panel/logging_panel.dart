import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/models/log.dart';
import 'package:Course_Work/services/logging_service.dart';

import 'package:Course_Work/components/log_panel/log_panel.dart';

import 'package:Course_Work/components/WindowManager.dart';

@Component(
    selector: 'logging-panel',
    styleUrls: const ['logging_panel.css'],
    templateUrl: 'logging_panel.html',
    directives: const [materialDirectives, LogPanel],
    providers: const [materialProviders])
class LoggingPanel extends WindowManager implements OnInit {
  @Input()
  List<Log> logs;

  void removeAll() {
    LoggingService.removeAllLogs();
  }

  @ViewChild('content')
  ElementRef content;

  @override
  void onResize() {
    _getNativeElement(content).style.maxHeight =
        (document.body.clientHeight - _getNativeElement(content).offset.top)
                .toString() +
            "px";
  }

  Element _getNativeElement(ElementRef element) {
    return element.nativeElement;
  }

  @override
  ngOnInit() {
    onResize();
  }
}
