import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/components/history_panel/history_panel.dart';

import 'package:Course_Work/services/delivery.dart';

import 'package:Course_Work/components/WindowManager.dart';

@Component(
    selector: 'delivery-history-panel',
    styleUrls: const ['delivery_history_panel.css'],
    templateUrl: 'delivery_history_panel.html',
    directives: const [materialDirectives, HistoryPanel],
    providers: const [materialProviders])
class DeliveryHistoryPanel extends WindowManager implements OnInit {
  @Input()
  List<Delivery> deliveries;

  @ViewChild('content')
  ElementRef content;

  @override
  void onResize() {
    _getNativeElement(content).style.maxHeight =
        (document.body.clientHeight - _getNativeElement(content).offset.top)
                .toString() +
            "px";
  }

  Element _getNativeElement(ElementRef element) {
    return element.nativeElement;
  }

  @override
  ngOnInit() {
    onResize();
  }
}
