import 'dart:html';

abstract class WindowManager {

  WindowManager() {
    setResizeEvent();
  }

  void setResizeEvent() {
    document.window.addEventListener("resize", (Event) => onResize());
  }

  void onResize();
}