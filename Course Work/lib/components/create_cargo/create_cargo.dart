import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/models/cargo.dart';
import 'package:Course_Work/services/delivery_service.dart';
import 'package:Course_Work/services/cargo_validation.dart';
import 'package:Course_Work/services/transport.dart';

@Component(
  selector: 'create-cargo',
  styleUrls: const ['create_cargo.css'],
  templateUrl: 'create_cargo.html',
  directives: const [materialDirectives, materialNumberInputDirectives],
  providers: const [materialProviders],
)

class CreateCargo {
  String from;
  String to;

  TransportType type;

  num weight;

  CreateCargo() {
    from = "Из ниоткуда";
    to = "В никуда";
    weight = 0;

    type = null;
  }

  void addCargo() {
    if(CargoValidation.isCargoValid(from, to, weight))
      DeliveryService.addDelivery(new Cargo(from, to, weight), type);
    if(CargoValidation.isCargoValid(from, to, weight))
      print(DeliveryService.getDeliveriesCount);
  }

  TransportType getTransportType(int index) {
    return TransportType.values[index];
  }

  void onTypeChange(dynamic index) {
    if(index is int) type = getTransportType(index);
    else type = index;
  }
}