import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/models/log.dart';
import 'package:Course_Work/services/logging_service.dart';

@Component(
    selector: 'log-panel',
    styleUrls: const ['log_panel.css'],
    templateUrl: 'log_panel.html',
    directives: const [materialDirectives, materialNumberInputDirectives],
    providers: const [materialProviders])
class LogPanel {
  @Input()
  Log log;

  ElementRef _elementRef;
  LogPanel(this._elementRef);

  void remove() {
    animate();
  }

  void animate() {
    (_elementRef.nativeElement as Element)
        .querySelector(".main")
        .setAttribute("state", "remove");
    (_elementRef.nativeElement as Element).animate([], 500)
      ..finished.then((e) => LoggingService.removeLog(log));
  }
}
