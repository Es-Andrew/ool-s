import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/services/delivery.dart';
import 'package:Course_Work/services/delivery_service.dart';
import 'package:Course_Work/components/cargo_panel/cargo_panel.dart';

@Component(
    selector: 'history-panel',
    styleUrls: const ['history_panel.css'],
    templateUrl: 'history_panel.html',
    directives: const [materialDirectives, CargoPanel],
    providers: const [materialProviders])
class HistoryPanel {
  @Input()
  Delivery delivery;

  void remove() {
    DeliveryService.removeDelivery(delivery);
  }
}
