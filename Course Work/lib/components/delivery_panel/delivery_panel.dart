import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/services/delivery.dart';
import 'package:Course_Work/components/cargo_panel/cargo_panel.dart';
import 'package:Course_Work/services/transport.dart';

@Component(
    selector: 'delivery-panel',
    styleUrls: const ['delivery_panel.css'],
    templateUrl: 'delivery_panel.html',
    directives: const [materialDirectives, CargoPanel],
    providers: const [materialProviders])
class DeliveryPanel {
  @Input()
  Delivery delivery;

  void run() {
    delivery.start();
  }

  String getIconName() {
    switch(delivery.transport.runtimeType.toString())
    {
      case "Vehicle" : return "local_shipping";
      case "Aircraft" : return "flight";
      case "Ship" : return "directions_boat";
      default: return "";
    }
  }

  String getTransportType(TransportType type) {
    return type.toString();
  }

}
