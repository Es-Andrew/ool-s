import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'package:Course_Work/services/delivery.dart';

import 'package:Course_Work/components/active_delivery_panel/active_delivery_panel.dart';

import 'package:Course_Work/components/WindowManager.dart';

@Component(
    selector: 'active-deliveries-panel',
    styleUrls: const ['active_deliveries_panel.css'],
    templateUrl: 'active_deliveries_panel.html',
    directives: const [materialDirectives, ActiveDeliveryPanel],
    providers: const [materialProviders])
class ActiveDeliveriesPanel extends WindowManager implements OnInit {
  @Input()
  List<Delivery> deliveries;

  @ViewChild('content')
  ElementRef content;

  @override
  void onResize() {
    _getNativeElement(content).style.maxHeight =
        (document.body.clientHeight - _getNativeElement(content).offset.top)
                .toString() +
            "px";
  }

  Element _getNativeElement(ElementRef element) {
    return element.nativeElement;
  }

  @override
  ngOnInit() {
    onResize();
  }
}
