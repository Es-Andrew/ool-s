﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02.Models
{
    class Circle : Shape
    {
        public override string Name { get; protected set; }
        public override double Parameter { get; protected set; }

        public Circle(double radius, string name) : base(radius, name)
        {

        }

        public override double GetArea()
        {
            return Math.PI * Math.Pow(Parameter, 2);
        }

        public override double GetPerimeter()
        {
            return 2 * Math.PI * Parameter;
        }
        
        public override int VerticesCount => 0;
        public override string Detail
        {
            get {
                return Name + "\n" + "Радиус: " + Parameter + "\n" + "Площадь: " + GetArea() + "\n" +
                     "Периметр: " + GetPerimeter();
            }
        }
    }
}
