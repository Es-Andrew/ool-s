﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02.Models
{
    class Square : Shape
    {
        public Square(double side, string name) : base(side, name)
        {
            
        }

        public override double GetArea()
        {
            return Math.Pow(Parameter, 2);
        }

        public override double GetPerimeter()
        {
            return Parameter*4;
        }

        public override string Name { get; protected set; }
        public override int VerticesCount => 4;

        public override string Detail
        {
            get
            {
                return Name + "\n" + "Длина стороны: " + Parameter + "\n" + "Площадь: " + GetArea() + "\n" +
                       "Периметр: " + GetPerimeter();
            }
        }

        public override double Parameter { get; protected set; }
    }
}
