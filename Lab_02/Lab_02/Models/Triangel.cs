﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02.Models
{
    class Triangel : Shape
    {
        public override double Parameter { get; protected set; }
        public override string Name { get; protected set; }

        public Triangel(double side, string name) : base(side, name)
        {

        }

        public override double GetArea()
        {
            return Math.Sqrt(3) * Math.Pow(Parameter, 2) / 4;
        }

        public override double GetPerimeter()
        {
            return Parameter * 3;
        }
        
        public override int VerticesCount => 3;
        public override string Detail
        {
            get {
                return Name + "\n" + "Длина стороны: " + Parameter + "\n" + "Площадь: " + GetArea() + "\n" +
                     "Периметр: " + GetPerimeter();
            }
        }
    }
}
