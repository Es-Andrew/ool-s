﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02.Models
{
    abstract class Shape
    {
        public abstract double Parameter { get; protected set; }
        public abstract string Name { get; protected set; }

        protected Shape(double parameter, string name)
        {
            if (!IsValide(parameter)) throw new ArgumentException("Incorrect value...");
            Parameter = parameter;
            Name = name;
        }

        protected bool IsValide(double parameter)
        {
            if (parameter <= 0) return false;
            return true;
        }

        public abstract double GetArea();
        public abstract double GetPerimeter();
        
        public abstract int VerticesCount { get; }
        public abstract string Detail { get; }
    }
}
