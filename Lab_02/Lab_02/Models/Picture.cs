﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02.Models
{
    class Picture
    {
        public List<Shape> Shapes { get; private set; }
        private Random _random;

        public Picture(int count)
        {
            _random = new Random();
            Shapes = new List<Shape>();
            for (int i = 0; i < count; i++)
            {
                int randomValue = _random.Next(0, 3);
                if(randomValue == 0) Shapes.Add(GetSquare(5,45));
                else if(randomValue == 1) Shapes.Add(GetCirce(5, 45));
                else Shapes.Add(GetTriangel(5,45));
            }
            Shapes.Sort(new ShapeComparer());
        }

        private Square GetSquare(int minSideValue, int maxSideValue)
        {
            return new Square(_random.Next(minSideValue, maxSideValue), "Square#" + _random.Next(0, maxSideValue));
        }

        private Circle GetCirce(int minSideValue, int maxSideValue)
        {
            return new Circle(_random.Next(minSideValue, maxSideValue), "Circle#" + _random.Next(0, maxSideValue));
        }

        private Triangel GetTriangel(int minSideValue, int maxSideValue)
        {
            return new Triangel(_random.Next(minSideValue, maxSideValue), "Triangel#" + _random.Next(0, maxSideValue));
        }

        public void ConsoleOutput()
        {
            foreach (Shape shape in Shapes)
            {
                Console.WriteLine("-----");
                Console.WriteLine(shape.Detail);
            }
        }
    }

    class ShapeComparer : IComparer<Shape>
    {
        public int Compare(Shape x, Shape y)
        {
            return (int)(x.Parameter - y.Parameter);
        }
    }
}
