﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01
{
    class Program
    {
        static void Main(string[] args)
        {
            ReverseOrderNumbers reverseOrderNumbers = new ReverseOrderNumbers("numbers.txt");
            reverseOrderNumbers.OutputNumbersToFile("reverseNumbers.txt");
        }
    }
}
