﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01
{
    class ReverseOrderNumbers
    {
        private string[] _numbers;
        private Stack<string> _numbersStack;
        private string _reverseNumbers;

        public ReverseOrderNumbers(string path)
        {
            GetNumbersFromFile(path);
            GetNumbersStack();
            GetReverseNumbers();
        }

        private void GetNumbersFromFile(string path)
        {
            _numbers = File.ReadAllText(path).Split(' ');
        }

        private void GetNumbersStack()
        {
            _numbersStack = new Stack<string>();
            foreach (string number in _numbers)
                _numbersStack.Push(number);
        }

        public void OutputNumbersToFile(string path)
        {
            File.WriteAllText(path, OutputNumbers);
        }

        private void GetReverseNumbers()
        {
            _reverseNumbers = "";
            for (int i = 0; i < _numbers.Length; i++)
                _reverseNumbers += _numbersStack.Pop() + " ";
        }

        public string OutputNumbers
        {
            get { return _reverseNumbers; }
        }
    }
}
