﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Lab_03
{
    class StudentsService
    {
        public ArrayList Students { get; private set; }

        public static StudentsService CreateFromJsonFile(string path = "data.json")
        {
            return new StudentsService(path);
        }

        private StudentsService(string path)
        {
            Students = GetInitializedStudents(path);
            Sort();
        }

        private ArrayList GetInitializedStudents(string path)
        {
            ArrayList outputStudents = new ArrayList();
            JToken[] inputStudents = GetStudentsFromJson(path);
            foreach (JToken student in inputStudents)
                outputStudents.Add(JsonConvert.DeserializeObject<Student>(student.ToString()));
            return outputStudents;
        }

        private static JToken[] GetStudentsFromJson(string jsonFile)
        {
            JObject jObject = JObject.Parse(File.ReadAllText(jsonFile));
            return jObject["Students"].Children().ToArray();
        }

        private void Sort()
        {
            Students.Sort();
        }

        public void PrintTopStudents()
        {
            int studentsCount = GetOptimalStudentsCount(3);
            for (int i = 0; i < studentsCount; i++)
            {
                PrintStudent(Students[i] as Student);
            }
        }

        private int GetOptimalStudentsCount(int maxValue)
        {
            if (Students.Count > maxValue) return maxValue;
            else return Students.Count;
        }

        private void PrintStudent(Student student)
        {
            Console.WriteLine("-----");
            Console.WriteLine("{0} {1} {2}", student.SurName, student.Name, student.Patronymic);
            Console.WriteLine("{0} {1}", student.Course, student.Group);
            Console.WriteLine("Result: {0}", student.Result.TimeOfDay);
        }
    }
}
