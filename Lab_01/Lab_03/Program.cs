﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentsService students = StudentsService.CreateFromJsonFile();
            students.PrintTopStudents();
            Console.Read();
        }
    }
}
