﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03
{
    class Student : IComparable
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Patronymic { get; set; }
        public int Course { get; set; }
        public string Group { get; set; }
        public DateTime Result { get; set; }
        public int CompareTo(object obj)
        {
            Student student = obj as Student;
            if (student == null) return 0;
            if (student.Result < Result) return 1;
            if (student.Result > Result) return -1;
            return 0;
        }
    }
}
