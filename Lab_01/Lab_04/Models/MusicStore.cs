﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Lab_04.Annotations;

namespace Lab_04.Models
{
    class MusicStore
    {
        private Hashtable _authors;
        public Hashtable Authors { get { return _authors; } set { _authors = value;} }

        public MusicStore()
        {
            Authors = new Hashtable();
        }

        public void AddAuthor(Author author)
        {
            try
            {
                Authors.Add(GetHashKey(author.Name), author);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }  
        }

        public void AddAlbum(Album inputAlbum, Author albumAuthor)
        {
            try
            {
                (Authors[GetHashKey(albumAuthor.Name)] as Author).Albums.Add(inputAlbum);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void AddSong(Song inputSong, Author songAuthor, Album songAlbum = null)
        {
            try
            {
                if(songAlbum == null) songAlbum = new Album("Singles");
                ObservableCollection<Album> authorAlbums = (Authors[GetHashKey(songAuthor.Name)] as Author).Albums;
                authorAlbums.First(album => album.Name == songAlbum.Name).Songs.Add(inputSong);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void RemoveAuthor(Author inputAuthor)
        {
            try
            {
                Authors.Remove(GetHashKey(inputAuthor.Name));
            }
            catch(Exception e) { Debug.Write(e); }
        }

        public void RemoveAlbum(Album inputAlbum, Author albumAuthor)
        {
            try
            {
                ObservableCollection<Album> albums = (Authors[GetHashKey(albumAuthor.Name)] as Author).Albums;
                albums.Remove(albums.First(album => album.Name == inputAlbum.Name));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void RemoveSong(Song inputSong, Album songAlbum, Author songAuthor)
        {
            try
            {
                ObservableCollection<Album> albums = (Authors[GetHashKey(songAuthor.Name)] as Author).Albums;
                ObservableCollection<Song> songs = albums.First(a => a.Name == songAlbum.Name).Songs;
                songs.Remove(songs.First(song => song.Name == inputSong.Name));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        private uint GetHashKey(string author)
        {
            uint result = 0;
            foreach (char symbol in author.ToLower())
            {
                result += GetHashFunctionValue(symbol, author.Length);
            }
            return result;
        }

        private uint GetHashFunctionValue(char symbol, int lenght)
        {
            uint hash = 0;
            for (uint i = 0; i < lenght; symbol++, i++)
                hash = (hash * 1664525) + symbol + 1013904223;
            return hash;
        }

        public ObservableCollection<Author> GetAuthors()
        {
            ObservableCollection<Author> result = new ObservableCollection<Author>();
            foreach (object author in Authors.Values)
                result.Add(author as Author);
            return result;
        }

        public ObservableCollection<Song> SearchResult(string authorName)
        {
            try
            {
                ObservableCollection<Song> songs = new ObservableCollection<Song>();
                ObservableCollection<Album> albums = (Authors[GetHashKey(authorName)] as Author).Albums;
                foreach (Album album in albums)
                    foreach (Song song in album.Songs)
                        songs.Add(song);
                return songs;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ObservableCollection<Song>();
            }
        }
    }
}
