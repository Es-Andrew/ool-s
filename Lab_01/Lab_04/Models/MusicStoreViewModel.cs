﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Lab_04.Annotations;

namespace Lab_04.Models
{
    class MusicStoreViewModel : INotifyPropertyChanged
    {
        private MusicStore musicStore;

        private Author selectedAuthor;
        private Album selectedAlbum;
        private Song selectedSong;

        private string _search;
        private ObservableCollection<Song> _searchResult;

        private RemoteCommand _addAuthorCommand;
        private RemoteCommand _addAlbumCommand;
        private RemoteCommand _addSongCommand;
        private RemoteCommand _removeAuthorCommand;
        private RemoteCommand _removeAlbumCommand;
        private RemoteCommand _removeSongCommand;

        private Author _addAuthor;
        private Album _addAlbum;
        private Song _addSong;

        public MusicStoreViewModel()
        {
            musicStore = new MusicStore();
            musicStore.AddAuthor(new Author("Tim mc.Morris"));
            musicStore.AddAuthor(new Author("Linking Park"));
            musicStore.AddAlbum(new Album("It's AMAZING"), new Author("Tim mc.Morris"));
            musicStore.AddSong(new Song("Live is Good"), new Author("Tim mc.Morris"), new Album("It's AMAZING"));
            AddAuthor = new Author("AddAuthor");
            AddAlbum = new Album("AddAlbum");
            AddSong = new Song("AddSong");
        }

        public Author SelectedAuthor {
            get { return selectedAuthor; }
            set
            {
                selectedAuthor = value;
                OnPropertyChanged();
            }
        }

        public Album SelectedAlbum
        {
            get { return selectedAlbum; }
            set
            {
                selectedAlbum = value;
                OnPropertyChanged();
            }
        }

        public Song SelectedSong
        {
            get { return selectedSong; }
            set
            {
                selectedSong = value;
                OnPropertyChanged();
            }
        }

        public string Search
        {
            get { return _search; }
            set
            {
                _search = value;
                OnPropertyChanged();
                SearchResult = musicStore.SearchResult(_search);
            }
        }

        public ObservableCollection<Song> SearchResult
        {
            get { return _searchResult; }
            set
            {
                _searchResult = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Author> GetAuthors
        { get
            { 
                return musicStore.GetAuthors();
            }
        }
        
        public RemoteCommand AddAuthorCommand
        {
            get
            {
                if (_addAuthorCommand != null) return _addAuthorCommand;
                _addAuthorCommand = new RemoteCommand(obj =>
                {
                    Author inputAuthor = new Author(AddAuthor.Name);
                    musicStore.AddAuthor(inputAuthor);
                    OnPropertyChanged("GetAuthors");
                });
                return _addAuthorCommand;
            }
        }

        public RemoteCommand AddAlbumCommand
        {
            get
            {
                if (_addAlbumCommand != null) return _addAlbumCommand;
                _addAlbumCommand = new RemoteCommand(obj =>
                {
                    Album inputAlbum = new Album(AddAlbum.Name);
                    musicStore.AddAlbum(inputAlbum, SelectedAuthor);
                    OnPropertyChanged("GetAuthors");
                });
                return _addAlbumCommand;
            }
        }

        public RemoteCommand AddSongCommand
        {
            get
            {
                if (_addSongCommand != null) return _addSongCommand;
                _addSongCommand = new RemoteCommand(obj =>
                {
                    Song inputSong = new Song(AddSong.Name);
                    musicStore.AddSong(inputSong, SelectedAuthor, SelectedAlbum);
                    OnPropertyChanged("GetAuthors");
                });
                return _addSongCommand;
            }
        }

        public RemoteCommand RemoveAuthorCommand
        {
            get
            {
                if(_removeAuthorCommand != null) return _removeAuthorCommand;
                return _removeAuthorCommand = new RemoteCommand(obj =>
                {
                    musicStore.RemoveAuthor(SelectedAuthor);
                    OnPropertyChanged("GetAuthors");
                });
            }
        }

        public RemoteCommand RemoveAlbumCommand
        {
            get
            {
                if (_removeAlbumCommand != null) return _removeAlbumCommand;
                return _removeAlbumCommand = new RemoteCommand(obj =>
                {
                    musicStore.RemoveAlbum(SelectedAlbum, SelectedAuthor);
                    OnPropertyChanged("GetAuthors");
                });
            }
        }

        public RemoteCommand RemoveSongCommand
        {
            get
            {
                if (_removeSongCommand != null) return _removeSongCommand;
                return _removeSongCommand = new RemoteCommand(obj =>
                {
                    musicStore.RemoveSong(SelectedSong, SelectedAlbum, SelectedAuthor);
                    OnPropertyChanged("GetAuthors");
                });
            }
        }

        public Author AddAuthor
        {
            get { return _addAuthor; }
            set
            {
                _addAuthor = value;
                OnPropertyChanged();
            }
        }

        public Album AddAlbum
        {
            get { return _addAlbum; }
            set
            {
                _addAlbum = value;
                OnPropertyChanged();
            }
        }

        public Song AddSong
        {
            get { return _addSong;}
            set
            {
                _addSong = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class RemoteCommand : ICommand
    {
        private Action<object> _execute;
        private Func<object, bool> _canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RemoteCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this._canExecute == null || this._canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this._execute(parameter);
        }
    }
}
