﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04.Models
{
    class Author
    {
        public string Name { get; set; }
        public ObservableCollection<Album> Albums { get; set; }

        public Author(string name)
        {
            Name = name;
            Albums = new ObservableCollection<Album>();
            Albums.Add(GetSinglesAlbum());
        }

        private Album GetSinglesAlbum()
        {
            return  new Album("Singles");
        }
    }
}
