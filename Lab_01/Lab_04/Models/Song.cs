﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04.Models
{
    class Song
    {
        public string Name { get; set; }

        public Song(string name)
        {
            Name = name;
        }
    }
}
