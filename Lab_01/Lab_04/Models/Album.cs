﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04.Models
{
    class Album
    {
        public string Name { get; set; }
        public ObservableCollection<Song> Songs { get; set; }

        public Album(string name)
        {
            Name = name;
            Songs = new ObservableCollection<Song>();
        }
    }
}
