﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Lab_02
{
    static class Students
    {
        private static JToken[] GetStudents(string jsonFile)
        {
            JObject jObject = JObject.Parse(File.ReadAllText(jsonFile));
            return jObject["Students"].Children().ToArray();
        }

        public static void ConsoleOutput(string jsonFile)
        {
            JToken[] jStudents = GetStudents(jsonFile);
            Queue<Student> badStudents = new Queue<Student>();
            foreach (JToken student in jStudents)
            {
                Student currentStudent = JsonConvert.DeserializeObject<Student>(student.ToString());
                bool isGoodSession = true;
                foreach (int number in currentStudent.Session)
                    if (number < 4) isGoodSession = false;
                if (isGoodSession == true) ConsoleOutput(currentStudent);
                else badStudents.Enqueue(currentStudent);
            }
            while (badStudents.Count>0)
                ConsoleOutput(badStudents.Dequeue());
        }

        private static void ConsoleOutput(Student student)
        {
            Console.WriteLine(student.Name);
        }
    }

    class Student
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string GroupNumber { get; set; }
        public int[] Session { get; set; }
    }
}
